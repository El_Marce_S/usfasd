<?php

class VencimientosReport extends CI_Controller
{

    public function Reporte()
    {

        $date = date('Ymd');
        $name = './Assets/ProfilePics/ReporteCedulasVencidas';


        $consulta1 = $this->db->query('SELECT	* FROM	hr_employee WHERE	active = "t" 	AND x_expiracio < CAST( CURRENT_TIMESTAMP + interval "7" day AS DATE ) ORDER BY x_expiracio ASC	');
        $rows = $consulta1->result();
        $resultado1 = array('result' => $rows);


        $consulta2 = $this->db->query('SELECT	* FROM	hr_employee WHERE	active = "t" 	AND x_expiracio IS NULL');
        $rows2 = $consulta2->result();
        $resultado2 = array('result' => $rows2);

        $this->load->view("Reports/VencimientoCI", $resultado1);
        $this->load->view("Reports/VencimientoCI2", $resultado2);
//gGet Output html
        $html = $this->output->get_output();
//load  pdf library
        $this->load->library('pdf');
//load  Html Content
        $this->dompdf->loadHtml($html);
//load  paper size
        $this->dompdf->setPaper('A4', 'Potrait');
//Render PDF
        $this->dompdf->render();
//outpup
        $this->dompdf->stream(date('Y-m-d H:i:s'), array('Attachment' => 0));
#$this->dompdf->output ('./Assets/ProfilePics/');
        $output = $this->dompdf->output();
        file_put_contents($name . $date . '.pdf', $output);
        #   $dompdf = new Dompdf();
        #  $dompdf->loadHtml($html);
        # $dompdf->render();
        #$this->dompdf->stream('test'.'.pdf');
    }
}