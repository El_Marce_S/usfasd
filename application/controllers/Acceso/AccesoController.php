<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AccesoController extends CI_Controller
{

    function index(){

    }

    public function __construct()
    {
        parent:: __construct();
        if (!$this->session->userdata("login")) {
            redirect('Auth');
        }
    }


    public function Home()
    {

        $this->load->view('LayoutsAcceso/0Header');
        $this->load->view('LayoutsAcceso/1SideMenu');
        $this->load->view('LayoutsAcceso/2TopNavigation');
        $this->load->view('LayoutsAcceso/Home');

    }

    public function Nueva_Invitacion()
    {

        $this->load->view('LayoutsAcceso/0Header');
        $this->load->view('LayoutsAcceso/1SideMenu');
        $this->load->view('LayoutsAcceso/2TopNavigation');
        $this->load->view('LayoutsAcceso/NewInvitation');
    }







    public function Guia_Telefonica()
    {
        $query=$this->db->query('select * from ListinTelefonico');
        $rows=$query->result();
        $info=array('result'=>$rows);

        $this->load->view('LayoutsAcceso/0Header');
        $this->load->view('LayoutsAcceso/1SideMenu');
        $this->load->view('LayoutsAcceso/2TopNavigation');
        $this->load->view('LayoutsAcceso/GuiaTelefonica',$info);
    }

    public function Agregar_Contactos()
    {
        $UsrCr = $this->session->userdata('id');

        if (isset($_POST['Guardar'])) {


            $dataReg = array
            (
                'Nombre' => $_POST['NOMBRE'],
                'TelefonoFijo1' => $_POST['Tel1'],
                'TelefonoFijo2' => $_POST['Tel2'],
                'TelIP' => $_POST['TelIP'],
                'Celular' => $_POST['Cel'],
                'Observacion' => $_POST['Obs'],
                'Dato2' => $_POST['Ref'],
                'Usr_Creator' => $UsrCr


            );

            $this->db->insert('ListinTelefonico', $dataReg);
            $this->session->set_flashdata('DONE','Conctacto agregado correctamente');
            redirect('Recepcion/RecepController/Agregar_Contactos');
        }

        $this->load->view('LayoutsAcceso/0Header');
        $this->load->view('LayoutsAcceso/1SideMenu');
        $this->load->view('LayoutsAcceso/2TopNavigation');
        $this->load->view('LayoutsAcceso/AgregarContactos');
    }
}