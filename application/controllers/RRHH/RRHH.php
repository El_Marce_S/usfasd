<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RRHH extends CI_Controller
{

    public function __construct()
    {
        parent:: construct();
        if(!isset($_SESSION ['user_logged'])){

            $this->session->set_flashdata ("error","Primero ingrese en esta pagina");
            redirect('Auth/LogIn');
        }
    }

    public function Test()
    {
        $this->load->view('welcome_message');
    }

    public function Home()
    {
        $this->load->view('Super_Admin/Admin_view');
    }

    public function RRHH()
    {
        $this->load->view('Super_Admin/RRHH_Home');
    }

    public function Crear_Persona()
    {
        if (isset($_POST['register'])) {
            $data = array(
                'Username' => $_POST['username'],
                'Password' => $_POST['password'],
                'UsuSis' => $_POST['SIST'],
                'UsuMailCorp' => $_POST['emailcorp'],
                'UsuEst' => $_POST['estado'],
            );
            $this->db->insert('Usuarios', $data);

            $this->session->set_flashdata("success", "La Cuenta ha sido Registrada, el usuario puede ingresar al sistema");
            redirect(base_url() . 'application/controllers/SAdmin/SAdmin/Home');
        }
        $this->load->view('Super_Admin/RH_CrearPrs_view');
    }

    public function Crear_Usuario()
    {
        if (isset($_POST['register'])) {
            $data = array(
                'Username' => $_POST['username'],
                'Password' => $_POST['password'],
                'UsuSis' => $_POST['SIST'],
                'UsuMailCorp' => $_POST['emailcorp'],
                'UsuEst' => $_POST['estado'],
            );
            $this->db->insert('Usuarios', $data);

            $this->session->set_flashdata("success", "La Cuenta ha sido Registrada, el usuario puede ingresar al sistema");
            redirect(base_url() . 'application/controllers/SAdmin/SAdmin/HomeSAdmin/SAdmin/home');
        }
        $this->load->view('Super_Admin/RH_CrearUsr_view');
    }
}
