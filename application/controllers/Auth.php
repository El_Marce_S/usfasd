<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Usuarios_model');
        $this->load->view('LogIn');
    }

    public function index()
    {
        if ($this->session->userdata('login')) {
            switch ($_SESSION['rol']) {
                case 0:
                    redirect('SAdmin/SAdmin/Home');
                    break;
                case 1:
                    redirect('welcome2');
                    break;
                case 33:
                    redirect('Recepcion/RecepController/Home');
                    break;
            }
        }
    }

    public function LogIn()
    {
        if (isset($_POST['password'])) {
            if ($this->Usuarios_model->LogIn($_POST['username'], $_POST['password'])) {
                $username = $this->input->post('username');
                $password = $this->input->post('password');
                $res = $this->Usuarios_model->LogIn($username, $password);
                if (!$res) {
                    $this->session->set_flashdata('error', "El usuario y/o contraseña son incorrectos");
                    redirect('Auth');
                } else {
                    $data = array
                    (
                        'id' => $res->idPersonal,
                        'Username' => $res->Username,
                        'rol' => $res->SistAsignado,
                        'Nombre' => $res->PerNom,
                        'Foto' => $res->PerFot,
                        'login' => TRUE
                    );
                    $this->session->set_userdata($data);
                    switch ($_SESSION['rol']) {
                        case 0:
                            redirect('SAdmin/SAdmin/Home');
                            break;
                        case 1:
                            redirect('welcome2');
                            break;
                        case 33:
                            redirect('Recepcion/RecepController/Home');
                            break;
                    }
                }
            }
        }
    }

    public function LogOut()
    {
        $this->session->sess_destroy();
        redirect('Auth');
    }
}
