<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Panel de Control </title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url() ?>Assets/Gentelella/vendors/bootstrap/dist/css/bootstrap.min.css"
          rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url() ?>Assets/Gentelella/vendors/font-awesome/css/font-awesome.min.css"
          rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url() ?>Assets/Gentelella/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<?php echo base_url() ?>Assets/Gentelella/build/css/custom.min.css" rel="stylesheet">
    <!-- bootstrap-datetimepicker -->
    <link href="<?php echo base_url() ?>Assets/Gentelella/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css"
          rel="stylesheet">
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="<?php echo base_url() ?>index.php/SAdmin/SAdmin/Home" class="site_title"><i
                                class="fa fa-home"></i> <span>Inicio</span></a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile clearfix">
                    <div class="profile_pic">
                        <img src="<?php echo base_url() ?>Assets/ProfilePics/<?php echo $this->session->userdata('Foto'); ?>"
                             alt="..." class="img-circle profile_img">

                    </div>
                    <div class="profile_info">
                        <span>Bienvenid@,</span>
                        <h2><?php echo $this->session->userdata('Nombre'); ?></h2>
                    </div>
                </div>
                <!-- /menu profile quick info -->

                <br/>

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <h3>Acceso a Sistemas</h3>
                        <ul class="nav side-menu">
                            <li><a><i class="fa fa-users"></i> RRHH <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="<?php echo base_url() ?>index.php/SAdmin/SAdmin/Crear_Persona">Crear
                                            Persona</a></li>
                                    <li><a href="<?php echo base_url() ?>index.php/SAdmin/SAdmin/Modificar_Persona">Modificar Persona</a></li>
                                    <li><a href="<?php echo base_url() ?>index.php/SAdmin/SAdmin/Crear_Usuario">Crear
                                            Usuario</a></li>
                                    <li><a href="index2.html">Modificar Usuario</a></li>
                                    <li><a href="index2.html">Vacaciones</a></li>
                                    <li><a href="index2.html">Papeletas y otras solicitudes</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-edit"></i> Forms <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="form.html">General Form</a></li>
                                    <li><a href="form_advanced.html">Advanced Components</a></li>
                                    <li><a href="form_validation.html">Form Validation</a></li>
                                    <li><a href="form_wizards.html">Form Wizard</a></li>
                                    <li><a href="form_upload.html">Form Upload</a></li>
                                    <li><a href="form_buttons.html">Form Buttons</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-desktop"></i> UI Elements <span
                                            class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="general_elements.html">General Elements</a></li>
                                    <li><a href="media_gallery.html">Media Gallery</a></li>
                                    <li><a href="typography.html">Typography</a></li>
                                    <li><a href="icons.html">Icons</a></li>
                                    <li><a href="glyphicons.html">Glyphicons</a></li>
                                    <li><a href="widgets.html">Widgets</a></li>
                                    <li><a href="invoice.html">Invoice</a></li>
                                    <li><a href="inbox.html">Inbox</a></li>
                                    <li><a href="calendar.html">Calendar</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-table"></i> Tables <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="tables.html">Tables</a></li>
                                    <li><a href="tables_dynamic.html">Table Dynamic</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-bar-chart-o"></i> Data Presentation <span
                                            class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="chartjs.html">Chart JS</a></li>
                                    <li><a href="chartjs2.html">Chart JS2</a></li>
                                    <li><a href="morisjs.html">Moris JS</a></li>
                                    <li><a href="echarts.html">ECharts</a></li>
                                    <li><a href="other_charts.html">Other Charts</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-clone"></i>Layouts <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="fixed_sidebar.html">Fixed Sidebar</a></li>
                                    <li><a href="fixed_footer.html">Fixed Footer</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="menu_section">
                        <h3>Live On</h3>
                        <ul class="nav side-menu">
                            <li><a><i class="fa fa-bug"></i> Additional Pages <span
                                            class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="e_commerce.html">E-commerce</a></li>
                                    <li><a href="projects.html">Projects</a></li>
                                    <li><a href="project_detail.html">Project Detail</a></li>
                                    <li><a href="contacts.html">Contacts</a></li>
                                    <li><a href="profile.html">Profile</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-windows"></i> Extras <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="page_403.html">403 Error</a></li>
                                    <li><a href="page_404.html">404 Error</a></li>
                                    <li><a href="page_500.html">500 Error</a></li>
                                    <li><a href="plain_page.html">Plain Page</a></li>
                                    <li><a href="login.html">Login Page</a></li>
                                    <li><a href="pricing_tables.html">Pricing Tables</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-sitemap"></i> Multilevel Menu <span
                                            class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="#level1_1">Level One</a>
                                    <li><a>Level One<span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu">
                                            <li class="sub_menu"><a href="level2.html">Level Two</a>
                                            </li>
                                            <li><a href="#level2_1">Level Two</a>
                                            </li>
                                            <li><a href="#level2_2">Level Two</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="#level1_2">Level One</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="javascript:void(0)"><i class="fa fa-laptop"></i> Landing Page <span
                                            class="label label-success pull-right">Coming Soon</span></a></li>
                        </ul>
                    </div>

                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="Settings">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Lock">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Logout"
                       href="<?php echo base_url(); ?>Auth/LogOut">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
                               aria-expanded="false">
                                <img src="images/img.jpg" alt=""><?php echo $this->session->userdata('Nombre'); ?>
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="javascript:;"> Perfil</a></li>
                                <li>
                                    <a href="javascript:;">
                                        <span class="badge bg-red pull-right">50%</span>
                                        <span>Settings</span>
                                    </a>
                                </li>
                                <li><a href="javascript:;">Help</a></li>
                                <li><a href="<?php echo base_url() ?>index.php/auth/LogOut"><i
                                                class="fa fa-sign-out pull-right"></i> Salir</a></li>
                            </ul>
                        </li>

                        <li role="presentation" class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown"
                               aria-expanded="false">
                                <i class="fa fa-envelope-o"></i>
                                <span class="badge bg-green">6</span>
                            </a>
                            <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                                <li>
                                    <a>
                                        <span class="image"><img src="images/img.jpg" alt="Profile Image"/></span>
                                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image"><img src="images/img.jpg" alt="Profile Image"/></span>
                                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image"><img src="images/img.jpg" alt="Profile Image"/></span>
                                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image"><img src="images/img.jpg" alt="Profile Image"/></span>
                                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <div class="text-center">
                                        <a>
                                            <strong>See All Alerts</strong>
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->


        <form action="<?php echo base_url() ?>index.php/SAdmin/SAdmin/Crear_Persona" method="post"
              enctype="multipart/form-data">


            <!-- page content -->
            <div class="right_col" role="main">
                <div class="clearfix"></div>

                <div class="col-md-8 col-sm-8 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2><i class="fa fa-bars"></i> Datos de Funcionario
                                <small>Ingrese Todos los datos</small>
                            </h2>

                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <div class="col-xs-3">
                                <!-- required for floating -->
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs tabs-left">
                                    <li class="active"><a href="#home" data-toggle="tab">Datos personales</a>
                                    </li>
                                    <li><a href="#profile" data-toggle="tab">Datos de contacto</a>
                                    </li>
                                    <li><a href="#messages" data-toggle="tab">Datos laborales</a>
                                    </li>
                                    <li><a href="#settings" data-toggle="tab">Finalizar</a>
                                    </li>
                                </ul>
                            </div>

                            <div class="col-xs-9">
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane active" id="home">
                                        <p class="lead">Datos Personales</p>
                                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                            <input type="text" name="NombreCompleto"
                                                   class="form-control has-feedback-left" id="inputSuccess2"
                                                   placeholder="Nombre Completo">
                                            <span class="fa fa-user form-control-feedback left"
                                                  aria-hidden="true"></span>
                                        </div>

                                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                            <input type="text" name="ApPat" class="form-control" id="inputSuccess3"
                                                   placeholder="Apellido Paterno">
                                            <span class="fa fa-user form-control-feedback right"
                                                  aria-hidden="true"></span>
                                        </div>

                                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                            <input type="text" name="ApMat"
                                                   class="form-control has-feedback-left" id="inputSuccess2"
                                                   placeholder="Apellido Materno"><span
                                                    class="fa fa-user form-control-feedback left"
                                                    aria-hidden="true"></span>
                                        </div>

                                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                            <input type="text" name="Cedula" class="form-control has-feedback-right"
                                                   id="inputSuccess4"
                                                   placeholder="Cedula de Identidad">
                                            <span class="fa fa-newspaper-o form-control-feedback right"
                                                  aria-hidden="true"></span>
                                        </div>


                                        <div class="form-group">
                                            <div class='col-sm-6'>
                                                Estado Civil
                                                <div class="form-group">
                                                    <select class="form-control" name="EstCivil">
                                                        <option value="">Sleccione una opción</option>
                                                        <option value="Casado">Casado</option>
                                                        <option value="Soltero">Soltero</option>
                                                        <option value="Divorciado">Divorciado</option>
                                                        <option value="Conviviente">Conviviente</option>
                                                        <option value="Viudo">Viudo</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class='col-sm-6'>
                                                Fecha de Nacimiento
                                                <div class="form-group">
                                                    <div class='input-group date' id='myDatepicker2'>
                                                        <input type='text' class="form-control" name="FechaNac"/>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                            <input type="text" name="NumHijos"
                                                   class="form-control has-feedback-left" id="inputSuccess2"
                                                   placeholder="Numero de hijos"><span
                                                    class="fa fa-group form-control-feedback left"
                                                    aria-hidden="true"></span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                            <input type="text" name="NumCta"
                                                   class="form-control has-feedback-left" id="inputSuccess2"
                                                   placeholder="Cuenta Bancaria"><span
                                                    class="fa fa-bank form-control-feedback right"
                                                    aria-hidden="true"></span>
                                        </div>

                                        <div class="form-group">
                                            <div class='col-sm-6'>
                                                Seleccione una Imágen
                                                <div class="form-group">
                                                    <div class="form-group">
                                                        <input type="file" name="foto_empleado" class="form-control">

                                                    </div>

                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="tab-pane " id="profile">
                                        <p class="lead">Datos de Contacto</p>
                                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                            <input type="text" name="Direc"
                                                   class="form-control has-feedback-left" id="inputSuccess2"
                                                   placeholder="Direccion Completa">
                                            <span class="fa fa-map-marker form-control-feedback left"
                                                  aria-hidden="true"></span>
                                        </div>

                                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                            <input type="text" name="Telf" class="form-control" id="inputSuccess3"
                                                   placeholder="Teléfono/ Celular">
                                            <span class="fa fa-phone-square form-control-feedback right"
                                                  aria-hidden="true"></span>
                                        </div>

                                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                            <input type="text" name="email"
                                                   class="form-control has-feedback-left" id="inputSuccess2"
                                                   placeholder="Correo electronico personal"><span
                                                    class="fa fa-at form-control-feedback left"
                                                    aria-hidden="true"></span>
                                        </div>

                                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                            <input type="text" name="CtcPrsn" class="form-control" id="inputSuccess3"
                                                   placeholder="Persona de Contacto">
                                            <span class="fa fa-user form-control-feedback right"
                                                  aria-hidden="true"></span>
                                        </div>

                                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                            <input type="text" name="Tel_Contact"
                                                   class="form-control has-feedback-left" id="inputSuccess2"
                                                   placeholder="Teléfono Persona de contacto"><span
                                                    class="fa fa-phone form-control-feedback left"
                                                    aria-hidden="true"></span>
                                        </div>


                                    </div>


                                    <div class="tab-pane" id="messages">
                                        <p class="lead">Datos Laborales</p>

                                        <div class='form-group'>
                                            <div class="col-sm-6">
                                                <div class='form-group'>
                                                    <BR>
                                                    <div class="form-group has-feedback">
                                                        <input type="text" name="Prof"
                                                               class="form-control has-feedback-left" id="inputSuccess2"
                                                               placeholder=" Profesion">
                                                        <span class="fa fa-map-marker form-control-feedback left"
                                                              aria-hidden="true"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class='col-sm-6'>
                                                Fecha de Presentación
                                                <div class="form-group">
                                                    <div class='input-group date' id='myDatepicker3'>
                                                        <input type='text' class="form-control" name="FechaPre"/>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                            <input type="number" name="NumItem"
                                                   class="form-control has-feedback-left" id="inputSuccess2"
                                                   placeholder="Numero de Item"><span
                                                    class="fa fa-file-text-o form-control-feedback left"
                                                    aria-hidden="true"></span>
                                        </div>

                                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                            <input type="number" name="NumCont"
                                                   class="form-control has-feedback-left" id="inputSuccess2"
                                                   placeholder="Numero de Contrato"><span
                                                    class="fa fa-file-text form-control-feedback left"
                                                    aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="settings">
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <h1 class=""> Verifique antes de finalizar.</h1>
                                            <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                                <button type="button" class="btn btn-primary">Cancelar</button>
                                                <button class="btn btn-primary" value="Limpiar" type="reset">Limpiar
                                                    Registros
                                                </button>
                                                <button type="submit" name="Guardar" value="Guardar"
                                                        class="btn btn-success">Registrar
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                </div>
        </form>
    </div>
    <!-- /page content -->

    <!-- footer content -->
    <footer>
        <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
        </div>
        <div class="clearfix"></div>
    </footer>
    <!-- /footer content -->
</div>
</div>

<!-- jQuery -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/nprogress/nprogress.js"></script>
<!-- jQuery Smart Wizard -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
<!-- Custom Theme Scripts -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/build/js/custom.min.js"></script>


<!-- bootstrap-daterangepicker -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/moment/min/moment.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap-datetimepicker -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<!-- Initialize datetimepicker -->

<script>
    $('#myDatepicker').datetimepicker({
        format: 'YYYY-MM-DD'
    });

    $('#myDatepicker2').datetimepicker({
        format: 'YYYY-MM-DD'
    });

    $('#myDatepicker3').datetimepicker({
        format: 'YYYY-MM-DD'
    });

</script>

</body>
</html>