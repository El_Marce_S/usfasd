<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Material Design Lite">





    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.teal-yellow.min.css"/>







    <!-- Data Picker estilos -->
    <link href="<?php echo base_url() ?>application/Scripts/dateselector/dateselector.css" rel="stylesheet">













</head>











<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header
            mdl-layout--fixed-tabs">
    <header class="mdl-layout__header">
        <div class="mdl-layout__header-row">
            <!-- Title -->
            <span class="mdl-layout-title">Registro Personal</span>
            <!-- Add spacer, to align navigation to the right -->
            <div  class="mdl-layout-spacer"></div>
            <!-- Navigation. We hide it in small screens. -->
            <nav  class="mdl-navigation ">
                <a class="mdl-navigation__link" href="">Ayuda</a>
                <a class="mdl-navigation__link" href="">Salir</a>

            </nav>
        </div>
        <!-- Tabs -->
        <div class="mdl-layout__tab-bar mdl-js-ripple-effect">
            <a href="#fixed-tab-1" class="mdl-layout__tab is-active">Datos Personales</a>
            <a href="#fixed-tab-2" class="mdl-layout__tab">Datos de Contacto</a>
            <a href="#fixed-tab-3" class="mdl-layout__tab">Datos Laborales</a>
        </div>
    </header>
    <div class="mdl-layout__drawer">
        <span class="mdl-layout-title">Title</span>
        <nav class="mdl-navigation">

            <a class="mdl-navigation__link" href="<?php echo base_url() ?>SAdmin/SAdmin/Home">

                <a class="mdl-navigation__link" href="<?php echo base_url() ?>application/controllers/SAdmin/SAdmin/Home"><i class="material-icons">account_circle</i>RRHH</a>
                <a class="mdl-navigation__link" href="<?php echo base_url() ?>SAdmin/SAdmin/Crear_Persona"><font color="#00675b">&nbsp;&nbsp;&nbsp;<i class="material-icons">account_circle</i>Crear Personal</font></a>
                <a class="mdl-navigation__link" href="<?php echo base_url() ?>SAdmin/SAdmin/Crear_Usuario">&nbsp;&nbsp;&nbsp;<i class="material-icons">account_circle</i>Crear Usuario</a>
                <a class="mdl-navigation__link" href="<?php echo base_url() ?>SAdmin/SAdmin/Home">&nbsp;&nbsp;&nbsp;<i class="material-icons">account_circle</i>Vacaciones</a>
                <a class="mdl-navigation__link" href="<?php echo base_url() ?>SAdmin/SAdmin/Home">&nbsp;&nbsp;&nbsp;<i class="material-icons">account_circle</i>Otras Funciones</a>

                <div class="mdl-navigation__link">
                    <hr>
                </div>

                <a class="mdl-navigation__link" href="<?php echo base_url() ?>SAdmin/SAdmin/Home"><i class="material-icons">business_center</i>Activos Fijos</a>
                <a class="mdl-navigation__link" href="<?php echo base_url() ?>SAdmin/SAdmin/Home">&nbsp;&nbsp;&nbsp;<i class="material-icons">business_center</i>Registrar Activo</a>
                <a class="mdl-navigation__link" href="<?php echo base_url() ?>SAdmin/SAdmin/Home">&nbsp;&nbsp;&nbsp;<i class="material-icons">business_center</i>Dar de Baja</a>
        </nav>
    </div>


    <body>
    <main class="mdl-layout__content">




        <form action="#" method="POST">
            <section class="mdl-layout__tab-panel is-active" id="fixed-tab-1">
                <div class="page-content">
                    <div class="mdl-grid"><!-- Empiezo Contenido de la primera pestanha-->

                        <div class="mdl-cell mdl-cell--1-col"></div>

                        <div class="mdl-cell mdl-cell--4-col">

                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <input class="mdl-textfield__input" name="nombre_pers" id="nombre_pers"/>
                                <label class="mdl-textfield__label" for="nombre_pers">Nombre Completo</label>
                            </div>


                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <input class="mdl-textfield__input" id="apellido_pat" name="apellido_pat" type="text"/>
                                <label class="mdl-textfield__label" for="apellido_pat">Apellido Paterno</label>
                            </div>


                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <input class="mdl-textfield__input" id="apellido_mat" name="apellido_mat" type="text"/>
                                <label class="mdl-textfield__label" for="apellido_mat">Apellido Materno</label>
                            </div>

                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <select class="mdl-textfield__input" id="EST_CIV" name="EST_CIV">
                                    <option></option>
                                    <option class="mdl-menu__item" value="01">Casado</option>
                                    <option class="mdl-menu__item" value="02">Soltero</option>
                                    <option class="mdl-menu__item" value="03">Divorciado</option>(
                                </select>
                                <label class="mdl-textfield__label" for="EST_CIV">Estado Civil</label>
                            </div>

                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <input class="mdl-textfield__input" id="cedula" name="cedula" type="text"/>
                                <label class="mdl-textfield__label" for="cedula">Cedula de Identidad</label>
                            </div>


                            <div class="mdl-textfield mdl-js-textfield">
                                <textarea class="mdl-textfield__input" type="text" rows= "3" id="sample5" ></textarea>
                                <label class="mdl-textfield__label" for="sample5">Direccion Completa</label>
                            </div>

                            <input class="demo" type="text">







                            <div class="mdl-cell mdl-cell--5-col">





                            </div>
                        </div>
            </section>

            <section class="mdl-layout__tab-panel" id="fixed-tab-2">
                <div class="page-content"><!-- Your content goes here --></div>
            </section>

            <section class="mdl-layout__tab-panel" id="fixed-tab-3">
                <div class="page-content"><!-- Your content goes here --></div>
            </section>
            <button  content=""  class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" name="register">
                Registrar
            </button>
        </form>
    </main>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
    <script src="<?php echo base_url() ?>application/Scripts/moment.js"></script>
    <script src="<?php echo base_url() ?>application/Scripts/dateselector/jquery.dateselector.js"></script>


    <script>
        $(document).ready(function () {
            alert('Estoy funcionando');
        });
        $('.demo').dateSelector({
            cssFramework: 'foundation',
        });
    </script>


    </body>
</div>




</html>



