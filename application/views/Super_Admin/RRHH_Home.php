<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-pink.min.css">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.teal-yellow.min.css" />
    <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
    <meta charset="utf-8">



    <title>Sistema RRHH</title>

</head>

<body>
<!-- Simple header with fixed tabs. -->
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header
            mdl-layout--fixed-tabs">
    <header class="mdl-layout__header">
        <div class="mdl-layout__header-row">
            <!-- Title -->
            <span class="mdl-layout-title">Sistema</span>
        </div>
        <!-- Navigation. We hide it in small screens. -->

        <!-- Tabs -->
        <div class="mdl-layout__tab-bar mdl-js-ripple-effect">
            <a href="#fixed-tab-1" class="mdl-layout__tab is-active">Tab 1</a>
            <a href="#fixed-tab-2" class="mdl-layout__tab">Tab 2</a>
            <a href="#fixed-tab-3" class="mdl-layout__tab">Tab 3</a>
        </div>
    </header>





    <div class="mdl-layout__drawer">
        <span class="mdl-layout-title">Bienvendio Usuarios Super Admin</span>
        <nav class="mdl-navigation">
            <div class="android-drawer-separator"></div>
            <a class="mdl-navigation__link" href="">Home</a>
            <div class="android-drawer-separator"></div>
            <a class="mdl-navigation__link" href="<?php echo base_url() ?>SAdmin/SAdmin/VISTA001">Pagina RRHH</a>
            <a class="mdl-navigation__link" href="">Link</a>
            <a class="mdl-navigation__link" href="">Link</a>
        </nav>
    </div>
    <main class="mdl-layout__content">
        <section class="mdl-layout__tab-panel is-active" id="fixed-tab-1">
            <div class="page-content"><!-- Your content goes here --></div>
        </section>
        <section class="mdl-layout__tab-panel" id="fixed-tab-2">
            <div class="page-content"><!-- Your content goes here --></div>
        </section>
        <section class="mdl-layout__tab-panel" id="fixed-tab-3">
            <div class="page-content"><!-- Your content goes here --></div>
        </section>
    </main>
</div>
</body>

</html>





