<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Empleados
                    <small>Personas que requieren creacion de usuarios</small>
                </h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">

                </div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Personas</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <p class="text-muted font-13 m-b-30">
                    A continuación encuentra una tabla de las personas recienmente creadas que necesitan habilitacion de
                    sistemas y creacion de usuarios para los mismos.
                </p>


                <table   id="datatable-responsive" border="1" cellspacing="0" cellpadding="5" class="table table-striped table-bordered dt-responsive nowrap">
                    <thead>

                    <th>Nombre Completo</th>
                    <th>Apellido Paterno</th>
                    <th>Apellido Materno</th>
                    <th>Cedula de Identidad</th>
                    <th>Accion</th>
                    </thead>


                    <tbody>
                    <?php
                    foreach ($result as $row) {

                    ?>
                    <tr>
                        <td><?php echo $row->PerNom ?></td>
                        <td><?php echo $row->PerApa ?></td>
                        <td><?php echo $row->PerAma ?></td>
                        <td><?php echo $row->PerCI ?></td>
                        <td  type="button" class=" btn btn-round btn-success boton" data-toggle="modal" data-target=".modalAsignar">Asignar Usuario</td>
                    </tr>
                    <?php }
                    ?>
                    </tbody>

                </table>

            </div>
        </div>
    </div>
</div>
</div>
</div>
<div class="modal fade modalAsignar" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel2">Asginación de Usuario</h4>
            </div>
            <div class="modal-body">
                <label for="username">Ingresar Nombre de Usuario para </label>
                <input type="text" name="username" id="username" class="form-control"><br>
                <label for="pass">Password</label>
                <input type="password" name="pass" id="pass" class="form-control"><br>
                <label for="ConfPass">Password</label>
                <input type="password" name="ConfPass" id="ConfPass" class="form-control"><br>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn-default" data-dismiss="modal">Atrás</button>
                <button type="button" class="btn btn-round btn-success" value="Asignar">Asignar Usuario</button>
            </div>

        </div>
    </div>
</div>
<!-- /page content -->

<!-- footer content -->
<footer>
    <div class="pull-right">
        Caja Petrolera de Salud La Paz <a href="https://colorlib.com">Unidad de Sistemas, Servicios Web y Desarollo de Sistemas</a>
    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->
</div>
</div>

<!-- jQuery -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/nprogress/nprogress.js"></script>
<!-- jQuery Smart Wizard -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
<!-- Custom Theme Scripts -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/build/js/custom.min.js"></script>


<!-- iCheck -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/iCheck/icheck.min.js"></script>
<!-- Datatables -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/jszip/dist/jszip.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/pdfmake/build/vfs_fonts.js"></script>

<script>
    $('#datatable-responsive').on('click', function(){

        /* Por cada columna */
        $('#lista-presentaciones tr').each(function(){

            /* Obtener todas las celdas */
            var celdas = $(this).find('td');

            /* Mostrar el valor de cada celda */
            celdas.each(function(){ alert($(this).html()); });

            /* Mostrar el valor de la celda 2 */
            alert( $(celdas[1]).html() );

        });
    });
</script>

<script>
    $(document).ready(function(){
        $(".boton").click(function(){
            var valores="";

            // Obtenemos todos los valores contenidos en los <td> de la fila
            // seleccionada
            $(this).parents("tr").find("td").each(function(){
                valores+=$(this).html()+"\n";
            });
            alert(valores);
        });
    });
</script>

</body>
</html>