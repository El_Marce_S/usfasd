<div class="row">
    <!-- accepted payments column -->
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Lista de Vencimientos de Cédula de Identidad a La Fecha<br>
                    <small>Cedulas Vencidas<p id="date"></p></small>
                </h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Codigo Empro</th>
                        <th>Nombre Completo</th>
                        <th>Carnet de Identidad</th>
                        <th>Expirado</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php

                    foreach ($result as $row) {
                        ?>
                        <tr>
                            <td><?php echo $row->x_employee_ ?></td>
                            <td><?php echo $row->x_legal_nam ?></td>
                            <td><?php echo $row->identificat ?></td>
                            <td><?php echo $row->x_expiracio ?></td>
                        </tr>

                    <?php }
                    ?>

            </div>
        </div>
    </div>
</div>

</div>
</div>
</div>
</div>
</div>


</div>
</div>
</div>
</div>
</div>
</div>
<!-- /page content -->

<!-- /footer content -->
</div>

</div>

<!-- jQuery -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/nprogress/nprogress.js"></script>
<!-- jQuery Smart Wizard -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
<!-- Custom Theme Scripts -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/build/js/custom.min.js"></script>


<!-- iCheck -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/iCheck/icheck.min.js"></script>
<!-- Datatables -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/jszip/dist/jszip.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/pdfmake/build/vfs_fonts.js"></script>


<script>
    document.getElementById("date").innerHTML = Date();
</script>


</body>

</html>