$('#document').ready(function () {
    $('#tabla_usuarios').html('<thead>\n' +
        '                    <tr>\n' +
        '                        <th>Nombre Completo</th>\n' +
        '                        <th>Apellido Paterno</th>\n' +
        '                        <th>Apellido Materno</th>\n' +
        '                        <th>Editar</th>\n' +
        '                        <th>Eliminar</th>\n' +
        '                    </tr>\n' +
        '                    </thead>');


    $.post(baseurl + "index.php/SAdmin/SAdmin/getPersonas",
        function (data) {
            var persona = JSON.parse(data);
            $.each(persona, function (i, row) {
                $('#tabla_usuarios').append(
                    '<tr>' +
                    '<td ">' + row.PerNom + '</td>' +
                    '<td>' + row.PerApa + '</td>' +
                    '<td>' + row.PerAma + '</td>' +
                    '<td align="center"><a href="#" class=" btn btn-round btn-success  boton" data-toggle="modal"  data-target="#ModalEditarPersona" onClick="selPersona(\'' + row.idPersonal + '\',\'' + row.PerNom + '\',\'' + row.PerApa + '\',\'' + row.PerAma + '\');">Modificar Datos</a>  </td>' +
                    '<td align="center"><a href="#" class=" btn btn-round btn-danger  boton" data-toggle="modal"  data-target="#ModalEditarPersona" onClick="selPersona(\'' + row.idPersonal + '\',\'' + row.PerNom + '\',\'' + row.PerApa + '\',\'' + row.PerAma + '\');">Eliminiar</a>  </td>' +
                    '</tr>'
                );
            });
        });
});

//con esta funcion pasamos los paremtros a los text del modal.
selPersona = function (idPersonal, PerNom, PerApa, PerAma) {


    $('#modal_idPersonal').val(idPersonal);
    $('#modal_PerNom').val(PerNom);
    $('#modal_PerApa').val(PerApa);
    $('#modal_PerAma').val(PerAma);

};